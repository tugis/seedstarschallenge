<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'
        ]);
    }

    /**
     * Create a new user instance. Password is not set.
     *
     * @param  Request  $request
     * @return User
     */
    protected function postAddUser(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect('/add')
                ->withInput()
                ->withErrors($validator);
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect('/add')
            ->with('success',
                sprintf('User %s (%s) created successfully.', $request->name, $request->email)
            );
    }

    /**
     * Returns a view with the list of Users
     *
     * @return \Illuminate\Http\Response
     */
    protected function showListUsers() {
        $users = User::all();

        return response()->view('listUsers',
            array(
                'users' => $users
            )
        );
    }
}
