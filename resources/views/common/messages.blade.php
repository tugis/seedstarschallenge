{{-- */ $success = Session::get('success') /* --}}
@if($success)
    <div class="alert-box alert-success">
        <strong>Success!</strong>

        <ul>
            <li>{{ $success }}</li>
        </ul>
    </div>
@endif

@if (count($errors) > 0)
<div class="alert-box alert-danger">
    <strong>Whoops! Something went wrong!</strong>

    <br><br>

    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif