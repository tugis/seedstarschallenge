# Seedstars Challenge

This Challenge app was done using [Laravel Framework](http://laravel.com/).

It consists on a simple app to create and list users (names and emails).

It uses [Bower](http://bower.io) to keep contributed assets up to date and [Gulp](http://gulpjs.com) to automate project tasks.

## Installation

1. Get node modules dependencies
```
npm install
```

2. Get the project PHP modules dependencies and execute artisan commands
```
composer update
```

3. Get the latest assets versions
```
bower update
```

4. Execute automation tasks
```
gulp copyfiles
gulp
```

5. Create .env configurations file
```
cp .env.example .env
```

6. Build schema (edit your .env file to set database credentials)
```
php artisan migrate
```