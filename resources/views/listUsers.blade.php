@extends('layouts.app')

@section('content')

    <h1>Users List</h1>

    <div class="panel-body">
        @if (count($users) > 0)
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Email</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                </tr>
            @endforeach
        </table>
        @else
            <p>There are no users stored yet. Create some <a href="/add">here</a></p>
        @endif

    </div>
@endsection