<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />

    <title>{{ config('app.name') }}</title>

    <!-- CSS And JavaScript -->
    <link href="/assets/css/public.css" rel="stylesheet">
    @yield('styles')

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container-fluid">
    @include('partials.navbar')

    <div class="jumbotron">
        @yield('content')
    </div>
</div>

<script src="/assets/js/public.js"></script>

<footer id="footer">
    <div class="container text-center">
        <p class="credit">Done by Luis Carneiro using <a href="http://laravel.com" target="_blank" tile="Laravel Page">Laravel</a></p>
    </div>
</footer>

@yield('scripts')

</body>
</html>