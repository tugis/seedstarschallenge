<header class="navbar navbar-default" id="top" role="banner">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar-menu"
                    aria-controls="bs-navbar" aria-expanded="true">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        </div>
        <nav id="navbar-menu" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Route::currentRouteNamed('list') ? 'active' : '' }}">
                    <a href="/list">List</a>
                </li>
                <li class="{{ Route::currentRouteNamed('add') ? 'active' : '' }}">
                    <a href="/add">Add</a>
                </li>
            </ul>
        </nav>
    </div>
</header>
